﻿#include <iostream>

using namespace std;

int main()
{
    int werte[10] = {7,3,1,27,8,6,12,4,9,14};

    cout << "Werte am Anfang" << endl;
    // Ausgeben
    for (int i = 0; i < 10; i++)
    {
        cout << werte[i] << endl;
    }


    // // Vertauschen
    // int tmp = werte[0];
    // werte[0] = werte[9];
    // werte[9] = tmp;


    //Umkehren
    cout << "Umgekehrt" << endl;
    for (int i = 0; i < 5; i++)
    {
        int tmp = werte[i];
        werte[i] = werte[9-i];
        werte[9-i] = tmp;
    }

    //Nochmal ausgeben
    for (int i = 0; i < 10; i++)
    {
        cout << werte[i] << endl;
    }
    
}